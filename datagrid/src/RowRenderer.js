DataGrid.RowRenderer = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'short',

	detailsClass: undefined,
	cells: [],

	initialize: function( args ) {
		this.columns = args.columns;
		this.gridID = args.gridID;
		this.detailsClass = args.expandRenderer;
	},

	render: function() {
		_.each( this.cells, function( cell ) {
			cell.remove();
		} );
		this.cells = [];
		this.columns.each( this.create, this );
		return this;
	},

	/**
	 * Create the column cell.
	 *
	 * @param column Column to create
	 */
	create: function( column ) {
		var cell = new ( column.get( 'cellRenderer' ) )( {
			gridID: this.gridID,
			model: this.model,
            collection: this.collection,
			column: column
		} );
		this.cells.push( cell );
		this.$el.append( cell.render().$el );
	},
} );
