DataGrid.Header = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'header row',

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
	},

	render: function() {
		// render all the columns
        this.$el.empty();
		this.columns.each( this.create, this );
		this.calculateWidth();
		return this;
	},

	/**
	 * The header is in charge of keeping the column model width updated.
	 *
	 * The width of each column is based on its priority.
	 */
	calculateWidth: function() {
		var sum = this.columns.reduce( function( memo, column ) {
			// calculate the total of the priority.
			return memo + column.get( 'priority' );
		}, 0 );

		this.columns.each( function( column ) {
			// column width based on priority.
			column.set( 'width', ( ( column.get( 'priority' ) / sum ) * 100 ) +'%' );
		} );
	},

	/**
	 * Given a column definition and index, create the HTML markup for the column header.
	 * Append to the $el.
	 *
	 * @param column
	 * @param index
	 */
	create: function( column, index ) {
        var header = new DataGrid.HeaderRenderer( {
            gridID: this.gridID,
            collection: this.collection,
            model: column
        } );

        column.set( {
            index: index,
            target: header
        } );

        this.listenTo( header, 'sort', this.onSort );
		this.$el.append( header.render().$el );
	},

    /**
     * Called when header requests to be utilized as sort column.
     *
     * @param model {Column} Contains column definition.
     */
    onSort: function( model ) {
        this.collection.comparator = _.partial( DataGrid.Column.SORT._compareHelper, model );
        this.collection.sort( { fieldName: model.get( 'fieldName' ) } );
    }
} );
