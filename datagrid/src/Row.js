DataGrid.Row = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'row',

	detailsClass: undefined,

	short: undefined,
	details: undefined,
	columns: undefined,

	events: {
		'click': 'onRowClick'
	},

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
		this.detailsClass = args.detailsClass;
	},

	render: function() {
		if ( this.short ) {
			// destroy old short.
			this.short.remove();
		}

		if ( this.details ) {
			// destroy old details.
			this.details.remove();
		}

		this.short = new DataGrid.RowRenderer( {
			gridID: this.gridID,
			model: this.model,
            collection: this.collection,
			columns: this.columns
		} );

		this.$el.append( this.short.render().$el );

		if ( this.detailsClass ) {
			// instantiate and render the expand renderer.
			this.details = new this.detailsClass( {
				model: this.model
			} );

			this.$el.append( this.details.render().$el );
			this.details.$el.hide();
		}

		return this;
	},

	/**
	 * The row has been clicked.  Expand the row and dispatch an event notifying that the
	 * 	row model has been clicked.
	 *
	 * @param event MouseEvent on the row.
	 */
	onRowClick: function( event ) {
        if ( this.details ) {
            // expand/collapse renderer is attached.
            this.toggle();
            return;
        }

        this.trigger( 'click', { model: this.model } );
	},

	key: function() {
		return 'row-'+ this.model.id;
	},

	/**
	 * Toggle the row's expand/collapse mode.  Perform this with animation, if available.
	 */
	toggle: function() {
		if ( this.isExpanded() ) {
			this.collapse();
		} else {
			this.expand();
		}
	},

    /**
     * @returns {boolean} True if the row is expanded, otherwise false.
     */
    isExpanded: function() {
      return DataGrid.load( this.gridID, this.key() +'-details' ) && true;
    },

	/**
	 * Expand this row's details pane
	 *
	 * @param {boolean} animate Whether or not to animate the expand
     * @return {boolean} True if the row is successfully expanded, otherwise false.
	 */
	expand: function( animate ) {
		if ( !this.details || this.isExpanded() ) {
			// nothing more to do.
			return false;
		}

		if ( animate ) {
			// animate
			this.details.$el.slideDown();
		} else {
			// display immediately
			this.details.$el.show();
		}

		DataGrid.store( this.gridID, this.key() +'-details', true );
        return true;
	},

	/**
	 * Collapse this row's details pane
	 *
	 * @param {boolean} animate Whether or not to animate the collapse
     * @return {boolean} True if the row is successfully collapsed, otherwise false.
	 */
	collapse: function( animate ) {
		if ( !this.details || !this.isExpanded() ) {
			// nothing more to do.
			return false;
		}

		if ( animate ) {
			// animate
			this.details.$el.slideUp();
		} else {
			// display immediately
			this.details.$el.hide();
		}

		DataGrid.delete( this.gridID, this.key() +'-details' );
        return true;
	}
} );
