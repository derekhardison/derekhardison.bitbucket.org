DataGrid.HeaderRenderer = Backbone.View.extend( {
	gridID: '',
	className: 'cell header',

    events: {
        'click': 'onSort'
    },

	initialize: function( args ) {
        this.gridID = args.gridID;

		this.listenTo( this.model, 'change:width', function( model ) {
				// update the width of the header.
				this.$el.width( model.get( 'width' ) );
			}, this )
			.listenTo( this.model, 'change:sortDirection', function( model ) {
				// update the sort direction indicator.
				this.sort( model.get( 'sortDirection' ) );
			} )
			.listenTo( this.collection, 'sort', this.onCollectionSort )
			.$el.width( this.model.get( 'width' ) );
	},

	render: function() {
		this.$el.html( this.model.get( 'label' ) );
        return this;
	},

    /**
     * @returns {string} Returns the storage sort key for this column.
     */
    key: function() {
        return 'sort-'+ this.model.get( 'label' );
    },

    /**
     * Header has been clicked, check if it's sortable.  If so, trigger an event
     * with the column definition.
     */
    sort: function( direction ) {
        if ( !this.model.isSortable() ) {
            // nothing more to do.
            return;
        }

        if ( direction ) {
            // store setting for this column.
            DataGrid.store( this.gridID, this.key(), direction );
			this.model.set( 'sortDirection', direction );

            // notify header that we wish to sort on this column.
            this.trigger( 'sort', this.model );
            this.$el.addClass( direction );

            return;
        }

        // return current setting
        return DataGrid.load( this.gridID, this.key() );
    },

	onCollectionSort: function( collection, options ) {
		if ( options.fieldName !== this.model.get( 'fieldName' ) ) {
			// assert: another field is currently used as the collection sort.
			this.$el.removeClass( DataGrid.Column.SORT.ASC );
			this.$el.removeClass( DataGrid.Column.SORT.DESC );

			DataGrid.delete( this.gridID, this.key() );
			this.model.set( 'sortDirection', undefined );
		}
	},

    onSort: function( options ) {
		this.sort( DataGrid.load( this.gridID, this.key() ) === DataGrid.Column.SORT.ASC ? DataGrid.Column.SORT.DESC : DataGrid.Column.SORT.ASC );
    }
} );
