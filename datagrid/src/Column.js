DataGrid.Column = Backbone.Model.extend( {
    defaults: function() {
        return {
            label: '',

            fieldName: '',
            functionRenderer: undefined,
            cellRenderer: DataGrid.CellRenderer,

            sortDirection: undefined,
            sort: DataGrid.Column.SORT.alphabetical,

            priority: 2,
            index: -1,
            target: undefined
        };
    },

    /**
     * @returns {boolean} True if the sortFunction attribute is set, otherwise false.
     */
    isSortable: function() {
        return _.isFunction( this.get( 'sort' ) );
    }
}, {
    SORT: {
        /**
         * Compares two strings case insensitive.
         *
         * @param fieldName Name of field to compare.
         * @param m1 Model 1
         * @param m2 Model 2
         * @returns {number} -1 if m1 is less than m2, 1 if m1 is greater than m2, otherwise 0.
         */
        alphabetical: function( fieldName, m1, m2 ) {
            return m1.get( fieldName ).toLowerCase().localeCompare( m2.get( fieldName ).toLowerCase() );
        },

        numerical: function( fieldName, m1, m2 ) {
            var v1 = parseFloat( m1.get( fieldName ) ),
                v2 = parseFloat( m2.get( fieldName ) );

            if ( v1 < v2 ) {
                return -1;
            } else if ( v1 > v2 ) {
                return 1
            } else {
                return 0;
            }
        },

        /**
         * Modifier for the column sort function.
         *
         * @param column {Column} Column definition model.
         * @param model1 {Backbone.Model} First model to compare
         * @param model2 {Backbone.Model} Second model to compare
         * @returns {number} Returns -1 if model1 < model2, 1 if model1 > model2, or 0 if model1 == model2.
         */
        _compareHelper: function( column, model1, model2 ) {
            return ( column.get( 'sortDirection' ) === DataGrid.Column.SORT.ASC ? 1 : -1 ) * column.get( 'sort' )( column.get( 'fieldName' ), model1, model2 );
        },

        ASC: 'asc',
        DESC: 'desc'
    }
} );
