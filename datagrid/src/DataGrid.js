var DataGrid;
DataGrid = Backbone.View.extend( {
	className: 'datagrid',
	tagName: 'div',

	header: undefined,
	body: undefined,

	/**
	 * Setup event listeners and initialize the Grid.
	 */
	initialize: function( args ) {
		this.columns = args.columns;

		// instantiate body and header
		this.header = new DataGrid.Header( {
            gridID: this.id,
            collection: this.collection,
			columns: this.columns
		} );

		this.body = new DataGrid.Body( {
            gridID: this.id,
			collection: this.collection,
			columns: this.columns,
			detailsClass: args.detailsClass,
            height: args.height
		} );
	},

	render: function() {
		this.$el.empty()
            .append( this.header.render().$el )
            .append( this.body.render().$el );

		return this;
	},

	/**
	 * Sets the height maximum height of the grid body.
	 *
	 * @param height {number} Height of the DataGrid body.
	 */
	height: function( height ) {
		this.body.$el.height( height );
	}
}, {
    key: function( id, key ) {
        return 'datagrid-'+ id +'-'+ key;
    },

	delete: function( id, key ) {
		// delete the key
		localStorage.removeItem( DataGrid.key( id, key ) );
	},

    store: function( id, key, value ) {
        // store a unique datagrid key based on this component's ID.
        localStorage.setItem( DataGrid.key( id, key ), value );
    },

    load: function( id, key ) {
        return localStorage.getItem( DataGrid.key( id, key ) );
    }
} );
