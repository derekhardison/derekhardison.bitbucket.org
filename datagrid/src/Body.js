DataGrid.Body = Backbone.View.extend( {
    gridID: '',
	tagName: 'div',
	className: 'body',

	rows: [],
	columns: undefined,
	detailsClass: undefined,

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
		this.detailsClass = args.detailsClass;
        this.listenTo( this.collection, 'sort', this.render )
            .listenTo( this.collection, 'add', this.onCollectionAdd )
            .listenTo( this.collection, 'remove', this.onCollectionRemove );

        if ( args.height ) {
            // force the height for scrolling
            this.$el.height( args.height );
        }
	},

	render: function() {
		_.each( this.rows, function( row ) {
			// remove the row.
			row.remove();
		} );
		this.rows = [];
		this.collection.each( this.create, this );
		return this;
	},

	/**
	 * Instantiate a row for each item in the collection.  Render the row and
	 *   append the row's element to this view.
	 *
	 * @param item Backbone model with data to display in the row.
	 */
	create: function( item ) {
		var row = new DataGrid.Row( {
            gridID: this.gridID,
			model: item,
            collection: this.collection,
			columns: this.columns,
			detailsClass: this.detailsClass
		} );

        this.rows.push( row );
		this.$el.append( row.render().$el );
	},

    /**
     * If attribute highlight is false, then the model will not be highlighted in the DataGrid.
     *
     * @param model Backbone model added to the collection.
     * @param collection Backbone.Collection
     * @param attributes Contains some parameters (such as index).
     */
    onCollectionRemove: function( model, collection, attributes ) {
        if ( this.highlight !== false ) {
            // highlight the recently deleted model.
            this.highlight( attributes.index );
        }

        this.rows[ attributes.index ].remove();
        this.rows.splice( attributes.index, 1 );
    },

    /**
     * If attribute highlight is false, then the model will not be highlighted in the DataGrid.
     *
     * @param model Backbone model added to the collection.
     * @param collection Backbone.Collection
     * @param attributes Contains some parameters (such as index).
     */
    onCollectionAdd: function( model, collection, attributes ) {
        if ( this.highlight !== false ) {
            // highlight the recently added model.
            this.highlight( attributes.index );
        }
    },

    /**
     * Highlight the specified row index briefly.
     *
     * @param index to highlight.
     */
    highlight: function( index ) {
        console.log( 'Highlight: '+ index );
    }
} );
