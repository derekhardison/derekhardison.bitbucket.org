DataGrid.CellRenderer = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'cell',

	column: undefined,

	initialize: function( args ) {
		this.column = args.column;
        this.gridID = args.gridID;
		this.listenTo( this.model, 'change:'+ this.column.get( 'fieldName' ), this.render )
            .listenTo( this.column, 'change:width', function( event ) {
			    this.$el.width( this.column.get( 'width' ) );
		    }, this )
            .$el.width( this.column.get( 'width' ) );
	},

	/**
	 * Listen for changes in the column definition.
	 *
	 * @param event Column definition change event.
	 */
	onDefinitionChange: function( event ) {
        // implement, if needed.
	},

	render: function() {
		if ( this.column.get( 'functionRenderer' ) ) {
			// pull value from a function that returns HTML.
			this.$el.html( this.column.get( 'functionRenderer' )( this.model, this.column ) );
			return this;
		}

		if ( this.column.get( 'fieldName' ) ) {
			// pull value from the model field.
			this.$el.html( this.model.get( this.column.get( 'fieldName' ) ) );
			return this;
		}

		this.$el.html( '&nbsp;' );
		return this;
	}
} );
