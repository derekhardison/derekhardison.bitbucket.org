### Quick Summary ###
The Backbone DataGrid is a reusable component to display data from a Backbone.Collection.  DataGrid is highly customizable and easy to incorporate into current applications.

### Features ###
* Extensible cell renderers (charts in cells!)
* Sortable by column.
* Remembers last sorted column (using localStorage).
* Render content for cells using a function returning HTML.
* Headers that do not scroll out of view while scrolling data.
* Expandable row view support!
* Automatic row highlighting when models are added/removed from the collection (optional).

### Requirements ###
* jQuery (http://jquery.com)
* UnderscoreJS (http://underscorejs.org)
* BackboneJS (http://backbonejs.org)

### Production files ###
Production files are located in the `dist` directory (there are two files).

### Contribution ###
CSS is compiled using SASS (`grunt sass`).  This requires ruby and the SASS gem.  Also, you’ll want grunt.  To concatenate into a single file execute `grunt`.

### Future Work ###
* Minify the code (easy).
* Load the saved DataGrid state.

### Example ###
http://derekhardison.bitbucket.org/datagrid/

### Version ###
* 0.5

### Contact ###
* derek@hardison.co