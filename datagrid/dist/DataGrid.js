var DataGrid;
DataGrid = Backbone.View.extend( {
	className: 'datagrid',
	tagName: 'div',

	header: undefined,
	body: undefined,

	/**
	 * Setup event listeners and initialize the Grid.
	 */
	initialize: function( args ) {
		this.columns = args.columns;

		// instantiate body and header
		this.header = new DataGrid.Header( {
            gridID: this.id,
            collection: this.collection,
			columns: this.columns
		} );

		this.body = new DataGrid.Body( {
            gridID: this.id,
			collection: this.collection,
			columns: this.columns,
			detailsClass: args.detailsClass,
            height: args.height
		} );
	},

	render: function() {
		this.$el.empty()
            .append( this.header.render().$el )
            .append( this.body.render().$el );

		return this;
	},

	/**
	 * Sets the height maximum height of the grid body.
	 *
	 * @param height {number} Height of the DataGrid body.
	 */
	height: function( height ) {
		this.body.$el.height( height );
	}
}, {
    key: function( id, key ) {
        return 'datagrid-'+ id +'-'+ key;
    },

	delete: function( id, key ) {
		// delete the key
		localStorage.removeItem( DataGrid.key( id, key ) );
	},

    store: function( id, key, value ) {
        // store a unique datagrid key based on this component's ID.
        localStorage.setItem( DataGrid.key( id, key ), value );
    },

    load: function( id, key ) {
        return localStorage.getItem( DataGrid.key( id, key ) );
    }
} );
;;DataGrid.Column = Backbone.Model.extend( {
    defaults: function() {
        return {
            label: '',

            fieldName: '',
            functionRenderer: undefined,
            cellRenderer: DataGrid.CellRenderer,

            sortDirection: undefined,
            sort: DataGrid.Column.SORT.alphabetical,

            priority: 2,
            index: -1,
            target: undefined
        };
    },

    /**
     * @returns {boolean} True if the sortFunction attribute is set, otherwise false.
     */
    isSortable: function() {
        return _.isFunction( this.get( 'sort' ) );
    }
}, {
    SORT: {
        /**
         * Compares two strings case insensitive.
         *
         * @param fieldName Name of field to compare.
         * @param m1 Model 1
         * @param m2 Model 2
         * @returns {number} -1 if m1 is less than m2, 1 if m1 is greater than m2, otherwise 0.
         */
        alphabetical: function( fieldName, m1, m2 ) {
            return m1.get( fieldName ).toLowerCase().localeCompare( m2.get( fieldName ).toLowerCase() );
        },

        numerical: function( fieldName, m1, m2 ) {
            var v1 = parseFloat( m1.get( fieldName ) ),
                v2 = parseFloat( m2.get( fieldName ) );

            if ( v1 < v2 ) {
                return -1;
            } else if ( v1 > v2 ) {
                return 1
            } else {
                return 0;
            }
        },

        /**
         * Modifier for the column sort function.
         *
         * @param column {Column} Column definition model.
         * @param model1 {Backbone.Model} First model to compare
         * @param model2 {Backbone.Model} Second model to compare
         * @returns {number} Returns -1 if model1 < model2, 1 if model1 > model2, or 0 if model1 == model2.
         */
        _compareHelper: function( column, model1, model2 ) {
            return ( column.get( 'sortDirection' ) === DataGrid.Column.SORT.ASC ? 1 : -1 ) * column.get( 'sort' )( column.get( 'fieldName' ), model1, model2 );
        },

        ASC: 'asc',
        DESC: 'desc'
    }
} );
;;DataGrid.ColumnList = Backbone.Collection.extend( {
    model: DataGrid.Column
} );
;;DataGrid.Header = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'header row',

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
	},

	render: function() {
		// render all the columns
        this.$el.empty();
		this.columns.each( this.create, this );
		this.calculateWidth();
		return this;
	},

	/**
	 * The header is in charge of keeping the column model width updated.
	 *
	 * The width of each column is based on its priority.
	 */
	calculateWidth: function() {
		var sum = this.columns.reduce( function( memo, column ) {
			// calculate the total of the priority.
			return memo + column.get( 'priority' );
		}, 0 );

		this.columns.each( function( column ) {
			// column width based on priority.
			column.set( 'width', ( ( column.get( 'priority' ) / sum ) * 100 ) +'%' );
		} );
	},

	/**
	 * Given a column definition and index, create the HTML markup for the column header.
	 * Append to the $el.
	 *
	 * @param column
	 * @param index
	 */
	create: function( column, index ) {
        var header = new DataGrid.HeaderRenderer( {
            gridID: this.gridID,
            collection: this.collection,
            model: column
        } );

        column.set( {
            index: index,
            target: header
        } );

        this.listenTo( header, 'sort', this.onSort );
		this.$el.append( header.render().$el );
	},

    /**
     * Called when header requests to be utilized as sort column.
     *
     * @param model {Column} Contains column definition.
     */
    onSort: function( model ) {
        this.collection.comparator = _.partial( DataGrid.Column.SORT._compareHelper, model );
        this.collection.sort( { fieldName: model.get( 'fieldName' ) } );
    }
} );
;;DataGrid.Body = Backbone.View.extend( {
    gridID: '',
	tagName: 'div',
	className: 'body',

	rows: [],
	columns: undefined,
	detailsClass: undefined,

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
		this.detailsClass = args.detailsClass;
        this.listenTo( this.collection, 'sort', this.render )
            .listenTo( this.collection, 'add', this.onCollectionAdd )
            .listenTo( this.collection, 'remove', this.onCollectionRemove );

        if ( args.height ) {
            // force the height for scrolling
            this.$el.height( args.height );
        }
	},

	render: function() {
		_.each( this.rows, function( row ) {
			// remove the row.
			row.remove();
		} );
		this.rows = [];
		this.collection.each( this.create, this );
		return this;
	},

	/**
	 * Instantiate a row for each item in the collection.  Render the row and
	 *   append the row's element to this view.
	 *
	 * @param item Backbone model with data to display in the row.
	 */
	create: function( item ) {
		var row = new DataGrid.Row( {
            gridID: this.gridID,
			model: item,
            collection: this.collection,
			columns: this.columns,
			detailsClass: this.detailsClass
		} );

        this.rows.push( row );
		this.$el.append( row.render().$el );
	},

    /**
     * If attribute highlight is false, then the model will not be highlighted in the DataGrid.
     *
     * @param model Backbone model added to the collection.
     * @param collection Backbone.Collection
     * @param attributes Contains some parameters (such as index).
     */
    onCollectionRemove: function( model, collection, attributes ) {
        if ( this.highlight !== false ) {
            // highlight the recently deleted model.
            this.highlight( attributes.index );
        }

        this.rows[ attributes.index ].remove();
        this.rows.splice( attributes.index, 1 );
    },

    /**
     * If attribute highlight is false, then the model will not be highlighted in the DataGrid.
     *
     * @param model Backbone model added to the collection.
     * @param collection Backbone.Collection
     * @param attributes Contains some parameters (such as index).
     */
    onCollectionAdd: function( model, collection, attributes ) {
        if ( this.highlight !== false ) {
            // highlight the recently added model.
            this.highlight( attributes.index );
        }
    },

    /**
     * Highlight the specified row index briefly.
     *
     * @param index to highlight.
     */
    highlight: function( index ) {
        console.log( 'Highlight: '+ index );
    }
} );
;;DataGrid.CellRenderer = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'cell',

	column: undefined,

	initialize: function( args ) {
		this.column = args.column;
        this.gridID = args.gridID;
		this.listenTo( this.model, 'change:'+ this.column.get( 'fieldName' ), this.render )
            .listenTo( this.column, 'change:width', function( event ) {
			    this.$el.width( this.column.get( 'width' ) );
		    }, this )
            .$el.width( this.column.get( 'width' ) );
	},

	/**
	 * Listen for changes in the column definition.
	 *
	 * @param event Column definition change event.
	 */
	onDefinitionChange: function( event ) {
        // implement, if needed.
	},

	render: function() {
		if ( this.column.get( 'functionRenderer' ) ) {
			// pull value from a function that returns HTML.
			this.$el.html( this.column.get( 'functionRenderer' )( this.model, this.column ) );
			return this;
		}

		if ( this.column.get( 'fieldName' ) ) {
			// pull value from the model field.
			this.$el.html( this.model.get( this.column.get( 'fieldName' ) ) );
			return this;
		}

		this.$el.html( '&nbsp;' );
		return this;
	}
} );
;;DataGrid.HeaderRenderer = Backbone.View.extend( {
	gridID: '',
	className: 'cell header',

    events: {
        'click': 'onSort'
    },

	initialize: function( args ) {
        this.gridID = args.gridID;

		this.listenTo( this.model, 'change:width', function( model ) {
				// update the width of the header.
				this.$el.width( model.get( 'width' ) );
			}, this )
			.listenTo( this.model, 'change:sortDirection', function( model ) {
				// update the sort direction indicator.
				this.sort( model.get( 'sortDirection' ) );
			} )
			.listenTo( this.collection, 'sort', this.onCollectionSort )
			.$el.width( this.model.get( 'width' ) );
	},

	render: function() {
		this.$el.html( this.model.get( 'label' ) );
        return this;
	},

    /**
     * @returns {string} Returns the storage sort key for this column.
     */
    key: function() {
        return 'sort-'+ this.model.get( 'label' );
    },

    /**
     * Header has been clicked, check if it's sortable.  If so, trigger an event
     * with the column definition.
     */
    sort: function( direction ) {
        if ( !this.model.isSortable() ) {
            // nothing more to do.
            return;
        }

        if ( direction ) {
            // store setting for this column.
            DataGrid.store( this.gridID, this.key(), direction );
			this.model.set( 'sortDirection', direction );

            // notify header that we wish to sort on this column.
            this.trigger( 'sort', this.model );
            this.$el.addClass( direction );

            return;
        }

        // return current setting
        return DataGrid.load( this.gridID, this.key() );
    },

	onCollectionSort: function( collection, options ) {
		if ( options.fieldName !== this.model.get( 'fieldName' ) ) {
			// assert: another field is currently used as the collection sort.
			this.$el.removeClass( DataGrid.Column.SORT.ASC );
			this.$el.removeClass( DataGrid.Column.SORT.DESC );

			DataGrid.delete( this.gridID, this.key() );
			this.model.set( 'sortDirection', undefined );
		}
	},

    onSort: function( options ) {
		this.sort( DataGrid.load( this.gridID, this.key() ) === DataGrid.Column.SORT.ASC ? DataGrid.Column.SORT.DESC : DataGrid.Column.SORT.ASC );
    }
} );
;;DataGrid.Row = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'row',

	detailsClass: undefined,

	short: undefined,
	details: undefined,
	columns: undefined,

	events: {
		'click': 'onRowClick'
	},

	initialize: function( args ) {
		this.columns = args.columns;
        this.gridID = args.gridID;
		this.detailsClass = args.detailsClass;
	},

	render: function() {
		if ( this.short ) {
			// destroy old short.
			this.short.remove();
		}

		if ( this.details ) {
			// destroy old details.
			this.details.remove();
		}

		this.short = new DataGrid.RowRenderer( {
			gridID: this.gridID,
			model: this.model,
            collection: this.collection,
			columns: this.columns
		} );

		this.$el.append( this.short.render().$el );

		if ( this.detailsClass ) {
			// instantiate and render the expand renderer.
			this.details = new this.detailsClass( {
				model: this.model
			} );

			this.$el.append( this.details.render().$el );
			this.details.$el.hide();
		}

		return this;
	},

	/**
	 * The row has been clicked.  Expand the row and dispatch an event notifying that the
	 * 	row model has been clicked.
	 *
	 * @param event MouseEvent on the row.
	 */
	onRowClick: function( event ) {
        if ( this.details ) {
            // expand/collapse renderer is attached.
            this.toggle();
            return;
        }

        this.trigger( 'click', { model: this.model } );
	},

	key: function() {
		return 'row-'+ this.model.id;
	},

	/**
	 * Toggle the row's expand/collapse mode.  Perform this with animation, if available.
	 */
	toggle: function() {
		if ( this.isExpanded() ) {
			this.collapse();
		} else {
			this.expand();
		}
	},

    /**
     * @returns {boolean} True if the row is expanded, otherwise false.
     */
    isExpanded: function() {
      return DataGrid.load( this.gridID, this.key() +'-details' ) && true;
    },

	/**
	 * Expand this row's details pane
	 *
	 * @param {boolean} animate Whether or not to animate the expand
     * @return {boolean} True if the row is successfully expanded, otherwise false.
	 */
	expand: function( animate ) {
		if ( !this.details || this.isExpanded() ) {
			// nothing more to do.
			return false;
		}

		if ( animate ) {
			// animate
			this.details.$el.slideDown();
		} else {
			// display immediately
			this.details.$el.show();
		}

		DataGrid.store( this.gridID, this.key() +'-details', true );
        return true;
	},

	/**
	 * Collapse this row's details pane
	 *
	 * @param {boolean} animate Whether or not to animate the collapse
     * @return {boolean} True if the row is successfully collapsed, otherwise false.
	 */
	collapse: function( animate ) {
		if ( !this.details || !this.isExpanded() ) {
			// nothing more to do.
			return false;
		}

		if ( animate ) {
			// animate
			this.details.$el.slideUp();
		} else {
			// display immediately
			this.details.$el.hide();
		}

		DataGrid.delete( this.gridID, this.key() +'-details' );
        return true;
	}
} );
;;DataGrid.RowRenderer = Backbone.View.extend( {
	gridID: '',
	tagName: 'div',
	className: 'short',

	detailsClass: undefined,
	cells: [],

	initialize: function( args ) {
		this.columns = args.columns;
		this.gridID = args.gridID;
		this.detailsClass = args.expandRenderer;
	},

	render: function() {
		_.each( this.cells, function( cell ) {
			cell.remove();
		} );
		this.cells = [];
		this.columns.each( this.create, this );
		return this;
	},

	/**
	 * Create the column cell.
	 *
	 * @param column Column to create
	 */
	create: function( column ) {
		var cell = new ( column.get( 'cellRenderer' ) )( {
			gridID: this.gridID,
			model: this.model,
            collection: this.collection,
			column: column
		} );
		this.cells.push( cell );
		this.$el.append( cell.render().$el );
	},
} );
