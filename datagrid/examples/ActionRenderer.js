/**
 * Custom cell renderer for the last column.
 */
var ActionRenderer = DataGrid.CellRenderer.extend( {
    template: undefined,

    events: {
        'click': 'onButtonClick'
    },

    /**
     * Overriding the parent initialization function
     */
    initialize: function() {
        // call parent
        DataGrid.CellRenderer.prototype.initialize.apply( this, arguments );

        // compile the template
        this.template = _.template( $( '#actionRendererTemplate' ).html() );
    },

    /**
     * Once again, always return `this`.  Perform any rendering for the cell here.
     */
    render: function() {
        this.$el.html( this.template( this.model.attributes ) );
        return this;
    },

    onButtonClick: function( event ) {
        this.collection.remove( this.model );
        return false;
    }
} );
