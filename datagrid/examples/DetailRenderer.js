/**
 * Optional renderer to display additional information
 *  when a row is clicked on.
 */
var DetailRenderer = Backbone.View.extend( {
    tagName: 'div',
    className: 'detail', // always use the detail className.

    // compile the template specified in <head> above.
    template: undefined,

    initialize: function() {
        // use {{}} to declare variables.
        _.templateSettings = {
            interpolate: /\{\{(.+?)\}\}/g
        };

        // compile the template.
        this.template = _.template( $( '#detailTemplate' ).html() );
    },

    /**
     * Render the template above with the attributes from
     *  this.model (passed in automatically by the DataGrid).
     *
     *  Always return `this`!
     */
    render: function() {
        this.$el.html( this.template( this.model.attributes ) );
        return this;
    }
} );
