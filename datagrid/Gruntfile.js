module.exports = function(grunt) {
    grunt.initConfig({
	    pkg: grunt.file.readJSON('package.json'),

		concat: {
			options: {
				separator: ';;'
			},

			dist: {
				src: [ 'src/DataGrid.js', 'src/Column.js', 'src/ColumnList.js', 'src/Header.js', 'src/Body.js', 'src/CellRenderer.js', 'src/HeaderRenderer.js', 'src/Row.js', 'src/RowRenderer.js' ],
				dest: 'dist/<%= pkg.name %>.js'
			}
		},

		sass: {
			dist: {
				files: {
					'dist/DataGrid.css': 'src/DataGrid.scss'
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.registerTask('default', ['sass']);

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', ['concat']);
};
